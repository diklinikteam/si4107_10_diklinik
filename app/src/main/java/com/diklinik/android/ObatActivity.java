package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.diklinik.android.firebase.Cart;
import com.diklinik.android.lib.Snowflake;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObatActivity extends AppCompatActivity {
    private GridView gridObat;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    private JSONArray arrayObat;

    CounterFab btnCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obat);

        gridObat = findViewById(R.id.gridObat);
        btnCart = findViewById(R.id.btnCart);

        firebaseAuth = FirebaseAuth.getInstance();

        getObat();
        getCart();
    }

    private void getCart(){
        try {
            databaseReference = FirebaseDatabase.getInstance().getReference();
            databaseReference.child("cart").orderByChild("id").equalTo(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    btnCart.setCount(Integer.parseInt(String.valueOf(dataSnapshot.getChildrenCount())));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch(Exception ex){
            ex.printStackTrace();

        }
    }

    private void getObat(){
        try{
            databaseReference = FirebaseDatabase.getInstance().getReference();
            databaseReference.child("obat").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();
                    String json = gson.toJson(dataSnapshot.getValue()).replace("null,", "");
                    try {
                        arrayObat = new JSONArray(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("JSON", json);

                    GridObatAdapter gridObatAdapter = new GridObatAdapter(getApplicationContext(), arrayObat);
                    gridObat.setAdapter(gridObatAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            firebaseUser = firebaseAuth.getCurrentUser();
            Log.d("ID", firebaseUser.getUid());
        }catch (Exception ex){
            ex.printStackTrace();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    public void toCart(View view) {
        startActivity(new Intent(this, CartActivity.class));
    }

    static class GridObatAdapter extends BaseAdapter {
        private Context context;
        private JSONArray jsonArray;

        private StorageReference storageReference;

        TextView txtNama, txtHarga;
        ImageView imgBanner;
        Button btnAdd;

        private FirebaseAuth firebaseAuth;
        private FirebaseUser firebaseUser;
        private DatabaseReference databaseReference;

        int jumlah = 0;

        GridObatAdapter(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;

            this.firebaseAuth = FirebaseAuth.getInstance();
            this.firebaseUser = firebaseAuth.getCurrentUser();
            this.databaseReference = FirebaseDatabase.getInstance().getReference();
        }

        @Override
        public int getCount() {
            return this.jsonArray.length();
        }

        @Override
        public JSONObject getItem(int position) {
            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject = jsonArray.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final View view = layoutInflater.inflate(R.layout.item_obat, null);
            final JSONObject jsonObject = getItem(position);
            txtNama = view.findViewById(R.id.txtNama);
            txtHarga = view.findViewById(R.id.txtHarga);
            imgBanner = view.findViewById(R.id.imgBanner);
            btnAdd = view.findViewById(R.id.btnAdd);

            storageReference = FirebaseStorage.getInstance().getReference();

            try{
                txtNama.setText(jsonObject.getString("nama"));
                storageReference.child("obat/" + jsonObject.getString("id") + "/obat.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String downloadUrl = uri.toString();
                        Picasso.get().load(downloadUrl).into(imgBanner);
                    }
                });
                txtHarga.setText(jsonObject.getString("harga"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

             btnAdd.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     try {
                         AlertDialog.Builder builder = new AlertDialog.Builder(view.getRootView().getContext());
                         builder.setTitle("Jumlah Obat");
                         final EditText input = new EditText(view.getRootView().getContext());
                         input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                         builder.setView(input);

                         builder.setPositiveButton("Tambah", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 jumlah += Integer.parseInt(input.getText().toString());
                                 try {
                                     Cart cart = new Cart(firebaseUser.getUid(), jsonObject.getString("id"), jsonObject.getString("nama"), String.valueOf(jumlah), String.valueOf(jumlah * Integer.parseInt(jsonObject.getString("harga"))));
                                     addCart(cart);
                                 } catch (JSONException e) {
                                     e.printStackTrace();
                                 }
                             }
                         });

                         builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 dialog.cancel();
                             }
                         });

                         builder.show();

                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }
             });
            return view;
        }

        void addCart(Cart cart){
            Snowflake snowflake = new Snowflake(10);
            databaseReference.child("cart").child(String.valueOf(snowflake.nextId())).setValue(cart).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(context, "Item berhasil ditambahkan ke cart", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, "Item gagal ditambahkan ke cart", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
