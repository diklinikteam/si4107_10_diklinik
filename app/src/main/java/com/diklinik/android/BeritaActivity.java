package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diklinik.android.firebase.Berita;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class BeritaActivity extends AppCompatActivity {
    private GridView gridBerita;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private JSONArray arrayBerita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        gridBerita = findViewById(R.id.gridBerita);
        getBerita();

    }

    private void getBerita(){
        try{
            databaseReference.child("berita").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();
                    String json = gson.toJson(dataSnapshot.getValue()).replace("null,", "");
                    try {
                        arrayBerita = new JSONArray(json);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("JSON", json);


                    GridBeritaAdapter gridBeritaAdapter = new GridBeritaAdapter(getApplicationContext(), arrayBerita);
                    gridBerita.setAdapter(gridBeritaAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            firebaseUser = firebaseAuth.getCurrentUser();
            Log.d("ID User", firebaseUser.getUid());
        } catch (Exception e) {
            e.printStackTrace();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    static class GridBeritaAdapter extends BaseAdapter{
        private Context context;
        private JSONArray jsonArray;

        private StorageReference storageReference;

        TextView txtJudul;
        ImageView imgBanner;

        GridBeritaAdapter(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;
        }

        @Override
        public int getCount() {
            return this.jsonArray.length();
        }

        @Override
        public JSONObject getItem(int position) {
            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject = jsonArray.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.item_berita, null);
            final JSONObject jsonObject = getItem(position);
            txtJudul = view.findViewById(R.id.txtJudul);
            imgBanner = view.findViewById(R.id.imgBanner);

            storageReference = FirebaseStorage.getInstance().getReference();

            try{
                txtJudul.setText(jsonObject.getString("judul"));
                storageReference.child("berita/" + jsonObject.getString("id") + "/banner.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String downloadUrl = uri.toString();
                        Picasso.get().load(downloadUrl).into(imgBanner);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BeritaSingleActivity.class);
                    try {
                        intent.putExtra("id", jsonObject.getString("id"));
                        intent.putExtra("judul", jsonObject.getString("judul"));
                        intent.putExtra("deskripsi", jsonObject.getString("deskripsi"));
                        context.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return view;
        }
    }
}
