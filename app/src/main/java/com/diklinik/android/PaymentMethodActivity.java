package com.diklinik.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PaymentMethodActivity extends AppCompatActivity {
    private TextView txtTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        txtTotal = findViewById(R.id.txtTotal);
        Intent intent = getIntent();

        txtTotal.setText(intent.getStringExtra("total"));
    }

    public void toHome(View view) {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
