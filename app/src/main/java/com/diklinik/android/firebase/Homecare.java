package com.diklinik.android.firebase;

public class Homecare {
    String id, keluhan, lama;

    public Homecare(){

    }

    public Homecare(String id, String keluhan, String lama) {
        this.id = id;
        this.keluhan = keluhan;
        this.lama = lama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public String getLama() {
        return lama;
    }

    public void setLama(String lama) {
        this.lama = lama;
    }
}
