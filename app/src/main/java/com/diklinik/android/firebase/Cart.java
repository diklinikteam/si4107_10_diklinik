package com.diklinik.android.firebase;

public class Cart {
    private String id, idObat, nama, jumlah, harga;

    public Cart(String id, String idObat, String nama, String jumlah, String harga) {
        this.id = id;
        this.idObat = idObat;
        this.nama = nama;
        this.jumlah = jumlah;
        this.harga = harga;
    }

    public String getIdObat() {
        return idObat;
    }

    public void setIdObat(String idObat) {
        this.idObat = idObat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
