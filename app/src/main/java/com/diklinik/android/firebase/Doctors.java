package com.diklinik.android.firebase;

public class Doctors {
    private String id, name, nohp, address, type;

    public Doctors(){

    }

    public Doctors(String id, String name, String type, String nohp, String address) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.nohp = nohp;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
