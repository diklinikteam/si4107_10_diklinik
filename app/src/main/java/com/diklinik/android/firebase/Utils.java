package com.diklinik.android.firebase;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;

public class Utils {

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private Context context;
    private ValueEventListener valueEventListener;

    public Utils(FirebaseAuth firebaseAuth, DatabaseReference databaseReference, Context context) {
        this.firebaseAuth = firebaseAuth;
        this.databaseReference = databaseReference;
        this.context = context;
    }

    public void getUserByID(String userID){
        databaseReference.child("profiles").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    Profiles profiles = dataSnapshot.getValue(Profiles.class);
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
