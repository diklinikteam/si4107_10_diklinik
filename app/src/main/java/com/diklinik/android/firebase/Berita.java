package com.diklinik.android.firebase;

public class Berita {
    String id, judul, deskripsi, image;

    public Berita(){}

    public Berita(String id, String judul, String deskripsi, String image) {
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
