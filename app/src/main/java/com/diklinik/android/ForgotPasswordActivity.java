package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.diklinik.android.firebase.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ForgotPasswordActivity extends AppCompatActivity {
    private TextInputLayout inEmailLayout;
    private TextInputEditText inEmail;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        inEmailLayout = findViewById(R.id.inEmailLayout);
        inEmail = findViewById(R.id.inEmail);

        inEmail.addTextChangedListener(new InputWatcher(inEmail));
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            firebaseUser = firebaseAuth.getCurrentUser();
            Log.d("ID", firebaseUser.getUid());
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public boolean isEmailExists(String email){
        status = false;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        Query query = databaseReference.child("users").orderByChild("email").equalTo(email);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    inEmailLayout.setErrorEnabled(false);
                    status = true;

                    firebaseAuth.sendPasswordResetEmail(inEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(ForgotPasswordActivity.this, "Email ganti password sudah terkirim", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else{
                    inEmailLayout.setErrorEnabled(true);
                    inEmailLayout.setError("Email tidak ditemukan");
                    requestFocus(inEmail);
                    status = false;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return status;
    }

    public void doForgotPass(View view) {
        String email = inEmail.getText().toString();

        if(!validateEmail(email)){
            return;
        }

        if(!isEmailExists(email)){
            return;
        }

    }


    private class InputWatcher implements TextWatcher {
        private View view;

        private InputWatcher(View view){this.view = view;}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()){
                case R.id.inEmail:
                    validateEmail(inEmail.getText().toString());
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    /**
     * Validating Email | isEmpty, isMatchPatterns
     * @param email
     * @return
     */
    private Boolean validateEmail(String email){
        if(email.isEmpty()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak boleh kosong");
            requestFocus(inEmail);
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak sesuai ketentuan");
            requestFocus(inEmail);
            return false;
        }
        inEmailLayout.setErrorEnabled(false);
        return true;
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
