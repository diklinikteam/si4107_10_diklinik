package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.os.Bundle;
import android.se.omapi.Session;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.diklinik.android.firebase.Doctors;
import com.diklinik.android.firebase.Profiles;
import com.diklinik.android.firebase.Users;
import com.diklinik.android.lib.SessionManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {
    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private TextView txtHeaderName, txtVerified, txtName, txtPhone, txtEmail, txtAddress;
    private CircleImageView imgProfile;
    private Profiles profiles;
    private Users users;
    private Doctors doctors;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        /**
         * TextView initializations
         */
        txtHeaderName = findViewById(R.id.text_name_header);
        txtVerified   = findViewById(R.id.text_verified);
        txtName       = findViewById(R.id.text_name);
        txtPhone      = findViewById(R.id.text_phone);
        txtEmail      = findViewById(R.id.text_email);
        txtAddress    = findViewById(R.id.text_alamat);

        /**
         * CircleImageView initialization
         */
        imgProfile = findViewById(R.id.image_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            firebaseUser = firebaseAuth.getCurrentUser();
            Toast.makeText(this, "Welcome back, " + firebaseUser.getEmail(), Toast.LENGTH_SHORT).show();

            getUserByID(firebaseUser.getUid());

            txtName.setText(firebaseAuth.getCurrentUser().getPhoneNumber());
            txtEmail.setText(firebaseAuth.getCurrentUser().getEmail());
        }catch(NullPointerException ex){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            SessionManager sessionManager = new SessionManager(this);
            sessionManager.clear();
            ex.printStackTrace();
        }
    }

    public void toEdit(View view){
        startActivity(new Intent(this, ProfileEditActivity.class));
    }

    private void getUserByID(String userID){
        databaseReference.child("users").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                SessionManager sessionManager = new SessionManager(getApplicationContext());
                users = dataSnapshot.getValue(Users.class);
                try{
                    if(users.getTipe().equals("USER")){
                        sessionManager.createUserType("USER");
                        getProfileByID(users.getId());
                    }else if(users.getTipe().equals("DOKTER")){
                        sessionManager.createUserType("DOKTER");
                        getDoctorByID(users.getId());
                    }
                }catch(NullPointerException ex){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    sessionManager.clear();
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getProfileByID(String userID){
        databaseReference.child("profiles").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    profiles = dataSnapshot.getValue(Profiles.class);
                    if(profiles.getName().isEmpty() || profiles.getName() == null){
                        startActivity(new Intent(getApplicationContext(), ProfileEditActivity.class));
                    }else{
                        txtName.setText(profiles.getName());
                        txtAddress.setText(profiles.getAddress());
                        txtEmail.setText(firebaseAuth.getCurrentUser().getEmail());
                        txtPhone.setText(profiles.getNohp());
                    }
                }catch (NullPointerException ex){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    sessionManager.clear();
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getDoctorByID(String userID){
        databaseReference.child("doctors").child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                try {
                    doctors = dataSnapshot.getValue(Doctors.class);
                    if(doctors.getName().isEmpty() || doctors.getName() == null){
                        SessionManager sessionManager = new SessionManager(getApplicationContext());
                        sessionManager.createDoctorType(doctors.getType());
                        startActivity(new Intent(getApplicationContext(), ProfileEditActivity.class));
                    }else{
                        txtName.setText(doctors.getName());
                        txtAddress.setText(doctors.getAddress());
                        txtEmail.setText(firebaseAuth.getCurrentUser().getEmail());
                        txtPhone.setText(doctors.getNohp());

                    }
                }catch (NullPointerException ex){
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                    SessionManager sessionManager = new SessionManager(getApplicationContext());
                    sessionManager.clear();
                    ex.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
