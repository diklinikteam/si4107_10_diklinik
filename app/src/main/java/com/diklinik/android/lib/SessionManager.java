package com.diklinik.android.lib;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    private final int SESSION_MODE = 0;

    private static final String PREF_NAME = "KonsultasiSession";
    public static final String KEY_TIPE_DOKTER = "TipeDokter";
    public static final String KEY_TIPE_USER = "TipeUser";


    public SessionManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, SESSION_MODE);
        editor = sharedPreferences.edit();
    }

    public void createDoctorType(String tipeDokter){
        editor.putString(KEY_TIPE_DOKTER, tipeDokter);
        editor.commit();
    }

    public String getDoctorType(){
        return sharedPreferences.getString(KEY_TIPE_DOKTER, "");
    }

    public void createKonsultasi(String tipeDokter){
        editor.putString(KEY_TIPE_DOKTER, tipeDokter);
        editor.commit();
    }

    public void createUserType(String userType){
        editor.putString(KEY_TIPE_USER, userType);
        editor.commit();
    }

    public String getUserType(){
        return sharedPreferences.getString(KEY_TIPE_USER, "");
    }

    public String getKonsultasi(){
        return sharedPreferences.getString(KEY_TIPE_DOKTER, "");
    }

    public void clear(){
        editor.clear();
        editor.commit();
    }
}
