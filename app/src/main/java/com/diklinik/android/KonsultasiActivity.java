package com.diklinik.android;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.diklinik.android.lib.SessionManager;

public class KonsultasiActivity extends AppCompatActivity {
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konsultasi);

        sessionManager = new SessionManager(this);
    }

    public void toUmum(View view) {
        sessionManager.createKonsultasi("UMUM");
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    public void toGigi(View view){
        sessionManager.createKonsultasi("GIGI");
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    public void toSpesialis(View view){
        sessionManager.createKonsultasi("SPESIALIS");
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }
}
