package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.diklinik.android.firebase.Doctors;
import com.diklinik.android.firebase.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterDoctorActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    private Doctors doctors;
    private Users users;

    private TextInputEditText inUsername, inEmail, inPassword, inRePassword;
    private TextInputLayout inUsernameLayout, inEmailLayout, inPasswordLayout, inRePasswordLayout;
    private Spinner spinnerTipe;
    private String doctorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_register_doctor);

        //TextInputLayout
        inUsernameLayout    = findViewById(R.id.inUsernameLayout);
        inEmailLayout       = findViewById(R.id.inEmailLayout);
        inPasswordLayout    = findViewById(R.id.inPasswordLayout);
        inRePasswordLayout  = findViewById(R.id.inRePasswordLayout);

        //EditText
        inUsername   = findViewById(R.id.inUsername);
        inEmail      = findViewById(R.id.inEmail);
        inPassword   = findViewById(R.id.inPassword);
        inRePassword = findViewById(R.id.inRePassword);

        spinnerTipe = findViewById(R.id.spinnerTipe);
        spinnerAdapter();
        spinnerTipe.setOnItemSelectedListener(this);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        inUsername.addTextChangedListener(new InputWatcher(inUsername));
        inEmail.addTextChangedListener(new InputWatcher(inEmail));
        inPassword.addTextChangedListener(new InputWatcher(inPassword));
        inRePassword.addTextChangedListener(new InputWatcher(inRePassword));
    }

    private void spinnerAdapter(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.tipe_dokter, android.R.layout.simple_spinner_dropdown_item);
        spinnerTipe.setAdapter(adapter);
    }

    public void doRegister(View view) {
        final String username, email, password, repassword, tipeDokter;
        username   = inUsername.getText().toString();
        email      = inEmail.getText().toString();
        password   = inPassword.getText().toString();
        repassword = inRePassword.getText().toString();
        tipeDokter = spinnerTipe.getSelectedItem().toString();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                    users = new Users(currentUser.getUid(), inUsername.getText().toString(), inEmail.getText().toString(), "DOKTER");
                    doctors = new Doctors(currentUser.getUid(), "", doctorType, "", "");
                    databaseReference.child("users").child(currentUser.getUid()).setValue(users);
                    databaseReference.child("doctors").child(currentUser.getUid()).setValue(doctors);
                    Toast.makeText(getApplicationContext(), "Hi, " + currentUser.getEmail(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Sign Up Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void toLogin(View view) {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        doctorType = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private class InputWatcher implements TextWatcher {
        private View view;

        private InputWatcher(View view){this.view = view;}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()){
                case R.id.inUsername:
                    validateUsername(inUsername.getText().toString());
                    break;
                case R.id.inEmail:
                    validateEmail(inEmail.getText().toString());
                    break;
                case R.id.inPassword:
                    validatePassword(inPassword.getText().toString());
                    break;
                case R.id.inRePassword:
                    validateRePassword(inPassword.getText().toString(), inRePassword.getText().toString());
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    /**
     * Validating Password | isEmpty, isEquals
     * @param password
     * @return
     */
    private Boolean validatePassword(String password){
        if(password.isEmpty()){
            inPasswordLayout.setErrorEnabled(true);
            inPasswordLayout.setError("Password tidak boleh kosong");
            requestFocus(inPassword);
            return false;
        }

        if(password.length() < 6){
            inPasswordLayout.setErrorEnabled(true);
            inPasswordLayout.setError("Password harus >= 6 karakter");
            requestFocus(inPassword);
            return false;
        }

        inPasswordLayout.setErrorEnabled(false);
        return true;
    }

    /**
     * Validate RePassword
     * @param password
     * @param repassword
     * @return
     */
    private Boolean validateRePassword(String password, String repassword){
        if(repassword.isEmpty()){
            inRePasswordLayout.setErrorEnabled(true);
            inRePasswordLayout.setError("Konfirmasi password tidak boleh kosong");
            requestFocus(inRePassword);
            return false;
        }

        if(!repassword.equals(password)){
            inRePasswordLayout.setErrorEnabled(true);
            inRePasswordLayout.setError("Konfirmasi password tidak sama");
            requestFocus(inRePassword);
            return false;
        }

        inRePasswordLayout.setErrorEnabled(false);
        return true;
    }

    /**
     * Validating Username | isEmpty
     * @param username
     * @return
     */
    private Boolean validateUsername(String username){
        if(username.isEmpty()){
            inUsernameLayout.setErrorEnabled(true);
            inUsernameLayout.setError("Username tidak boleh kosong");
            requestFocus(inUsername);
            return false;
        }

        if(username.length() < 6){
            inUsernameLayout.setErrorEnabled(true);
            inUsernameLayout.setError("Username harus >= 6 karakter");
            requestFocus(inUsername);
            return false;
        }

        inUsernameLayout.setErrorEnabled(false);
        return true;
    }

    /**
     * Validating Email | isEmpty, isMatchPatterns
     * @param email
     * @return
     */
    private Boolean validateEmail(String email){
        if(email.isEmpty()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak boleh kosong");
            requestFocus(inEmail);
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak sesuai ketentuan");
            requestFocus(inEmail);
            return false;
        }
        inEmailLayout.setErrorEnabled(false);
        return true;
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
