package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.diklinik.android.firebase.Homecare;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomecareActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private Homecare homecare;

    private TextInputLayout inKeluhanLayout, inLamaSakitLayout;
    private TextInputEditText inKeluhan, inLamaSakit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homecare);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        inKeluhan = findViewById(R.id.inKeluhan);
        inLamaSakit = findViewById(R.id.inLamaSakit);

        inKeluhanLayout = findViewById(R.id.inKeluhanLayout);
        inLamaSakitLayout = findViewById(R.id.inLamaSakitLayout);

        inKeluhan.addTextChangedListener(new InputWatcher(inKeluhan));
        inLamaSakit.addTextChangedListener(new InputWatcher(inLamaSakit));
    }


    @Override
    protected void onStart() {
        super.onStart();
        try{
            firebaseUser = firebaseAuth.getCurrentUser();
            Log.d(String.valueOf(getApplicationContext()), firebaseUser.getUid());
        }catch(NullPointerException ex){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            ex.printStackTrace();
        }
    }

    public void doKirim(View view) {
        String keluhan, lamaSakit;
        keluhan = inKeluhan.getText().toString();
        lamaSakit = inLamaSakit.getText().toString();

        if(!validateKeluhan(keluhan)){
            return;
        }

        if(!validateLamaSakit(lamaSakit)){
            return;
        }

        homecare = new Homecare(firebaseUser.getUid(), keluhan, lamaSakit);
        createKeluhan(homecare);
    }

    private void createKeluhan(Homecare homecare){
        try {
            databaseReference.child("homecares").child(firebaseUser.getUid()).setValue(homecare).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(HomecareActivity.this, "Keluhan berhasil dikirim", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();
                    }else{
                        Toast.makeText(HomecareActivity.this, "Keluhan gagal dikirim", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(NullPointerException ex){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            ex.printStackTrace();
        }
    }


    private boolean validateKeluhan(String keluhan){
        if(keluhan.isEmpty()){
            inKeluhanLayout.setErrorEnabled(true);
            inKeluhanLayout.setError("Keluhan tidak boleh kosong");
            requestFocus(inKeluhan);
            return false;
        }

        inKeluhanLayout.setErrorEnabled(false);
        return true;
    }

    private boolean validateLamaSakit(String lamaSakit){
        if(lamaSakit.isEmpty()){
            inLamaSakitLayout.setErrorEnabled(true);
            inLamaSakitLayout.setError("Lama sakit tidak boleh kosong");
            requestFocus(inLamaSakit);
            return false;
        }

        inLamaSakitLayout.setErrorEnabled(false);
        return true;
    }

    private class InputWatcher implements TextWatcher {
        private View view;

        private InputWatcher(View view){this.view = view;}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()){
                case R.id.inKeluhan:
                    validateKeluhan(inKeluhan.getText().toString());
                    break;
                case R.id.inLamaSakit:
                    validateLamaSakit(inLamaSakit.getText().toString());
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
