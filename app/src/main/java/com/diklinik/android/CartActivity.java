package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class CartActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private JSONArray arrayCart;

    private GridView gridCart;
    private TextView txtTotal;
    private Button btnBayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        gridCart = findViewById(R.id.gridCart);
        txtTotal = findViewById(R.id.txtTotal);
        btnBayar = findViewById(R.id.btnBayar);

        getCart();

    }

    private void getCart() {
        try {
            databaseReference = FirebaseDatabase.getInstance().getReference();
            databaseReference.child("cart").orderByChild("id").equalTo(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();

                    List<Object> list = new ArrayList<>();
                    Map<String, List<String>> map = new HashMap<>();

                    try {
                        for (DataSnapshot parent : dataSnapshot.getChildren()) {
                            for (DataSnapshot child : parent.getChildren()) {
                                if(map.containsKey(child.getKey())){
                                    map.get(child.getKey()).add(child.getValue().toString());
                                }else{
                                    List<String> list1 = new ArrayList<>();
                                    list1.add(child.getValue().toString());
                                    map.put(child.getKey(), list1);
                                }
                            }
                            list.add(map);
                        }

                        String json = gson.toJson(list);
                        Log.d("JSON CART", json);
                        arrayCart = new JSONArray(list);
                        int total = 0;
                        for(int i = 0; i < arrayCart.length(); i++){
                            total += Integer.parseInt(arrayCart.getJSONObject(i).getJSONArray("harga").getString(i));
                        }
                        txtTotal.setText("Rp " + total);
                    } catch (NullPointerException | JSONException e) {
                        e.printStackTrace();
                    }
                    GridCartAdapter gridCartAdapter = new GridCartAdapter(getApplicationContext(), arrayCart);
                    gridCart.setAdapter(gridCartAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        try {
            Log.d("ID", firebaseUser.getUid());
        } catch (Exception ex) {
            ex.printStackTrace();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    public void toPembayaran(View view) {
        Intent intent = new Intent(this, PaymentMethodActivity.class);
        intent.putExtra("total", txtTotal.getText());
        startActivity(intent);
    }

    static class GridCartAdapter extends BaseAdapter {
        private Context context;
        private JSONArray jsonArray;

        private StorageReference storageReference;

        TextView txtNama, txtHarga;
        ImageView imgObat, btnMinus, btnPlus;
        EditText inJumlah;


        private FirebaseAuth firebaseAuth;
        private FirebaseUser firebaseUser;
        private DatabaseReference databaseReference;

        int jumlah = 0;

        GridCartAdapter(Context context, JSONArray jsonArray) {
            this.context = context;
            this.jsonArray = jsonArray;

            this.firebaseAuth = FirebaseAuth.getInstance();
            this.firebaseUser = firebaseAuth.getCurrentUser();
            this.databaseReference = FirebaseDatabase.getInstance().getReference();
        }

        @Override
        public int getCount() {
            return this.jsonArray.length();
        }

        @Override
        public JSONObject getItem(int position) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = jsonArray.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final View view = layoutInflater.inflate(R.layout.item_cart, null);
            final JSONObject jsonObject = getItem(position);
            txtNama = view.findViewById(R.id.txtNama);
            txtHarga = view.findViewById(R.id.txtHarga);
            imgObat = view.findViewById(R.id.imgObat);
            inJumlah = view.findViewById(R.id.inJumlah);
            btnMinus = view.findViewById(R.id.btnMinus);
            btnPlus = view.findViewById(R.id.btnPlus);

            storageReference = FirebaseStorage.getInstance().getReference();

            try {
                txtNama.setText(jsonObject.getJSONArray("nama").getString(position));
                storageReference.child("obat/" + jsonObject.getJSONArray("idObat").getString(position) + "/obat.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        String downloadUrl = uri.toString();
                        Picasso.get().load(downloadUrl).into(imgObat);
                    }
                });
                txtHarga.setText(jsonObject.getJSONArray("harga").getString(position));
                inJumlah.setText(jsonObject.getJSONArray("jumlah").getString(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return view;
        }
    }
}
