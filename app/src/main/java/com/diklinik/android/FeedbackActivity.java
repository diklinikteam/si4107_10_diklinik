package com.diklinik.android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class FeedbackActivity extends AppCompatActivity {
    private RatingBar ratingBar;
    private TextInputLayout txtSaranLayout;
    private EditText txtSaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        ratingBar = findViewById(R.id.ratingBar);
        txtSaran  = findViewById(R.id.txtSaran);

    }

    public void kirimSaran(View view) {
        Toast.makeText(this, "Saran Terkirim", Toast.LENGTH_SHORT).show();
    }
}