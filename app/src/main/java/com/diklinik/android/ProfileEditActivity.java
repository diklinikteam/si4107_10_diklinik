package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.diklinik.android.firebase.Doctors;
import com.diklinik.android.firebase.Profiles;
import com.diklinik.android.lib.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileEditActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;

    private Profiles profiles;
    private Doctors doctors;

    private TextInputLayout inNameLayout, inNoHpLayout, inAlamatLayout;
    private TextInputEditText inName, inNoHp, inAlamat;

    private SessionManager sessionManager;

    private String tipeDokter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        inNameLayout = findViewById(R.id.inNameLayout);
        inNoHpLayout = findViewById(R.id.inNoHpLayout);
        inAlamatLayout = findViewById(R.id.inAlamatLayout);

        inName = findViewById(R.id.inName);
        inNoHp = findViewById(R.id.inNoHp);
        inAlamat = findViewById(R.id.inAlamat);

        inName.addTextChangedListener(new InputWatcher(inName));
        inNoHp.addTextChangedListener(new InputWatcher(inNoHp));
        inAlamat.addTextChangedListener(new InputWatcher(inAlamat));

        sessionManager = new SessionManager(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            firebaseUser = firebaseAuth.getCurrentUser();
            Log.d(String.valueOf(getApplicationContext()), firebaseUser.getUid());
            tipeDokter = sessionManager.getDoctorType();
        }catch(NullPointerException ex){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            ex.printStackTrace();
        }
    }

    public void doSimpan(View view) {
        String name, alamat, nohp, userType;

        name = inName.getText().toString();
        alamat = inAlamat.getText().toString();
        nohp = inNoHp.getText().toString();
        userType = sessionManager.getUserType();

        if(!validateName(name)){
            return;
        }

        if(!validateAlamat(alamat)){
            return;
        }

        if(!validateNoHp(nohp)){
            return;
        }


        if(userType.equals("USER")){
            profiles = new Profiles(firebaseUser.getUid(), name, nohp, alamat);
            updateProfiles(profiles);
        }else{
            doctors = new Doctors(firebaseUser.getUid(), name, tipeDokter, nohp, alamat);
            updateDoctors(doctors);
        }
    }

    private void updateDoctors(Doctors doctors){
        try {
            databaseReference.child("doctors").child(firebaseUser.getUid()).setValue(doctors).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ProfileEditActivity.this, "Profile Berhasil diubah", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ProfileEditActivity.this, ProfileActivity.class));
                        finish();
                    }else{
                        Toast.makeText(ProfileEditActivity.this, "Profile Gagal diubah", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(NullPointerException ex){
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            sessionManager.clear();
            ex.printStackTrace();
        }
    }

    private void updateProfiles(Profiles profiles){
        try {
            databaseReference.child("profiles").child(firebaseUser.getUid()).setValue(profiles).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(ProfileEditActivity.this, "Profile Berhasil diubah", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ProfileEditActivity.this, ProfileActivity.class));
                        finish();
                    }else{
                        Toast.makeText(ProfileEditActivity.this, "Profile Gagal diubah", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }catch(NullPointerException ex){
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
            SessionManager sessionManager = new SessionManager(getApplicationContext());
            sessionManager.clear();
            ex.printStackTrace();
        }
    }

    private class InputWatcher implements TextWatcher {
        private View view;

        private InputWatcher(View view){this.view = view;}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()){
                case R.id.inName:
                    validateName(inName.getText().toString());
                    break;
                case R.id.inNoHp:
                    validateNoHp(inNoHp.getText().toString());
                    break;
                case R.id.inAlamat:
                    validateAlamat(inAlamat.getText().toString());
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


    private Boolean validateName(String name){
        if(name.isEmpty()){
            inNameLayout.setErrorEnabled(true);
            inNameLayout.setError("Nama tidak boleh kosong");
            requestFocus(inName);
            return false;
        }

        if(name.length() < 5){
            inNameLayout.setErrorEnabled(true);
            inNameLayout.setError("Nama harus >= 5 karakter");
            requestFocus(inName);
            return false;
        }

        inNameLayout.setErrorEnabled(false);
        return true;
    }

    private Boolean validateNoHp(String nohp){
        if(nohp.isEmpty()){
            inNoHpLayout.setErrorEnabled(true);
            inNoHpLayout.setError("No HP tidak boleh kosong");
            requestFocus(inNoHp);
            return false;
        }

        if(nohp.length() != 12 && nohp.length() != 13){
            inNoHpLayout.setErrorEnabled(true);
            inNoHpLayout.setError("No HP harus 12 sampai 13 angka");
            requestFocus(inNoHp);
            return false;
        }

        inNoHpLayout.setErrorEnabled(false);
        return true;
    }

    private Boolean validateAlamat(String alamat){
        if(alamat.isEmpty()){
            inNoHpLayout.setErrorEnabled(true);
            inNoHpLayout.setError("Alamat tidak boleh kosong");
            requestFocus(inNoHp);
            return false;
        }

        inNoHpLayout.setErrorEnabled(false);
        return true;
    }


    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
