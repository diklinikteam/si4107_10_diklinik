package com.diklinik.android;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;

    private TextInputLayout inEmailLayout, inPasswordLayout;
    private TextInputEditText inEmail, inPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        //TextInputlayout
        inEmailLayout = findViewById(R.id.inEmailLayout);
        inPasswordLayout = findViewById(R.id.inPasswordLayout);

        //TextInputEditText
        inEmail = findViewById(R.id.inEmail);
        inPassword = findViewById(R.id.inPassword);

        inEmail.addTextChangedListener(new InputWatcher(inEmail));
        inPassword.addTextChangedListener(new InputWatcher(inPassword));
    }

    public void toRegister(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    public void doLogin(View view) {
        String email, password;

        email    = inEmail.getText().toString();
        password = inPassword.getText().toString();

        if(!validateEmail(email)){
            return;
        }

        if(!validatePassword(password)){
            return;
        }

        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Authentication Failed!", Toast.LENGTH_SHORT).show();
                    return;
                }

                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                finish();
            }
        });
    }

    public void toForgotPass(View view) {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    private class InputWatcher implements TextWatcher {
        private View view;

        private InputWatcher(View view){this.view = view;}

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()){
                case R.id.inEmail:
                    validateEmail(inEmail.getText().toString());
                    break;
                case R.id.inPassword:
                    validatePassword(inPassword.getText().toString());
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    /**
     * Validating Email | isEmpty, isMatchPatterns
     * @param email
     * @return
     */
    private Boolean validateEmail(String email){
        if(email.isEmpty()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak boleh kosong");
            requestFocus(inEmail);
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            inEmailLayout.setErrorEnabled(true);
            inEmailLayout.setError("Email tidak sesuai ketentuan");
            requestFocus(inEmail);
            return false;
        }
        inEmailLayout.setErrorEnabled(false);
        return true;
    }

    /**
     * Validating Password | isEmpty, isEquals
     * @param password
     * @return
     */
    private Boolean validatePassword(String password){
        if(password.isEmpty()){
            inPasswordLayout.setErrorEnabled(true);
            inPasswordLayout.setError("Password tidak boleh kosong");
            requestFocus(inPassword);
            return false;
        }

        if(password.length() < 6){
            inPasswordLayout.setErrorEnabled(true);
            inPasswordLayout.setError("Password harus >= 6 karakter");
            requestFocus(inPassword);
            return false;
        }

        inPasswordLayout.setErrorEnabled(false);
        return true;
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
