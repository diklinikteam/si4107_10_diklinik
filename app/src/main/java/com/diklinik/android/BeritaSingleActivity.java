package com.diklinik.android;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class BeritaSingleActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    private ImageView imgBanner;
    private TextView txtDeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita_single);

        imgBanner = findViewById(R.id.imgBanner);
        txtDeskripsi = findViewById(R.id.txtDeskripsi);

        Intent intent = getIntent();

        setTitle(intent.getStringExtra("judul"));
        storageReference = FirebaseStorage.getInstance().getReference();
        storageReference.child("berita/" + intent.getStringExtra("id") + "/banner.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(imgBanner);
            }
        });

        txtDeskripsi.setText(intent.getStringExtra("deskripsi"));
    }
}
