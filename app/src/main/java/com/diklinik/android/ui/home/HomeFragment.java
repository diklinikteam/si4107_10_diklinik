package com.diklinik.android.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.diklinik.android.BeritaActivity;
import com.diklinik.android.HomecareActivity;
import com.diklinik.android.KonsultasiActivity;
import com.diklinik.android.LoginActivity;
import com.diklinik.android.ObatActivity;
import com.diklinik.android.R;
import com.google.firebase.auth.FirebaseAuth;

public class HomeFragment extends Fragment {
    private FirebaseAuth firebaseAuth;
    private ImageButton btnKonsultasi, btnHomecare, btnObat;
    private HomeViewModel homeViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        btnKonsultasi = root.findViewById(R.id.btnKonsultasi);
        btnHomecare = root.findViewById(R.id.btnHomecare);
        btnObat = root.findViewById(R.id.btnObat);

        firebaseAuth = FirebaseAuth.getInstance();

        btnKonsultasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), BeritaActivity.class));
            }
        });

        btnHomecare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), HomecareActivity.class));
            }
        });

        btnObat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), ObatActivity.class));
            }
        });

        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        return root;
    }
}
